<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('password');
            $table->boolean('dark_mode')->nullable()->default(null);
            $table->string('locale')->default(config('app.locale'));
            $table->boolean('is_admin')->default(false);
            $table->string('profile_photo_url');
            $table->dropColumn('email_verified_at');
            $table->dropColumn('two_factor_secret');
            $table->dropColumn('two_factor_recovery_codes');
            $table->dropColumn('remember_token');
            $table->dropColumn('profile_photo_path');
            $table->softDeletes();
        });
        Schema::table('teams', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password');
            $table->dropColumn('dark_mode');
            $table->dropColumn('locale');
            $table->dropColumn('is_admin');
            $table->dropColumn('profile_photo_url');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->text('profile_photo_path')->nullable();
            $table->text('two_factor_secret')->nullable();
            $table->text('two_factor_recovery_codes')->nullable();
            $table->dropSoftDeletes();
        });
        Schema::table('teams', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
};
