<p align="center">
    <img src="https://be-to.nl/images/round.png" alt="BeTo Logo" style="background-color: transparent"/>
</p>

# SSO for Laravel

![Version](https://img.shields.io/packagist/v/beto/laravel-sso?style=for-the-badge&label=Version&link=https%3A%2F%2Fpackagist.org%2Fpackages%2Fbeto%2Flaravel-sso)
![PHP Version](https://img.shields.io/packagist/php-v/beto/laravel-sso?style=for-the-badge&label=PHP&link=https%3A%2F%2Fwww.php.net%2Freleases%2F8.2%2Fen.php)
![Laravel Version](https://img.shields.io/packagist/dependency-v/beto/laravel-sso/laravel%2Fframework?style=for-the-badge&label=Laravel&link=https%3A%2F%2Flaravel.com%2F)
![Jetstream Version](https://img.shields.io/packagist/dependency-v/beto/laravel-sso/laravel%2Fjetstream?style=for-the-badge&label=Laravel&link=https%3A%2F%2Flaravel.com%2F)

[![GitLab](https://gitlab.com/berkvenstoonen/laravel/laravel-sso/badges/main/pipeline.svg?key_text=Pipeline)](https://gitlab.com/berkvenstoonen/laravel/laravel-sso)
[![Latest Stable Version](http://poser.pugx.org/beto/laravel-sso/v)](https://packagist.org/packages/beto/laravel-sso)
[![Total Downloads](http://poser.pugx.org/beto/laravel-sso/downloads)](https://packagist.org/packages/beto/laravel-sso)
[![License](http://poser.pugx.org/beto/laravel-sso/license)](https://packagist.org/packages/beto/laravel-sso)
[![Laravel Version](http://poser.pugx.org/beto/laravel-sso/require/laravel%2Fframework)](https://laravel.com/)
[![Jetstream Version](http://poser.pugx.org/beto/laravel-sso/require/laravel%2Fjetstream)](https://jetstream.laravel.com/introduction.html)
[![PHP Version](http://poser.pugx.org/beto/laravel-sso/require/php)](https://packagist.org/packages/beto/laravel-sso)
![Contributions Welcome](https://img.shields.io/badge/contributions-welcome-green.svg?link=https%3A%2F%2Fgitlab.com%2Fberkvenstoonen%2Flaravel%2Fplugins%2Fsso)

## Setup

Setup the `.env` variables:

- `SSO_SERVER_URL`
- `SSO_PLATFORM_ID`
- `SSO_PLATFORM_KEY`

~~~
composer require beto/laravel-sso
php artisan vendor:publish --tag beto-sso
~~~

Update `config/fortify.php` with the following values (these are configured in the SSO Server)

~~~
    'views' => false,
    'features' => [],
~~~
