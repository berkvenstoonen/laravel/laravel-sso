<?php

return [
    'managed-externally' => [
        'label'       => 'Extern Profiel',
        'description' => 'Dit project wordt beheerd in :source.',
        'action'      => 'Open profiel in :source',
    ],
];
