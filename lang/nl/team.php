<?php

return [
    'managed-externally' => [
        'label'       => 'Externe Groep',
        'description' => 'Deze groep wordt beheerd in :source.',
        'action'      => 'Open groep in :source',
    ],
];
