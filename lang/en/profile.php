<?php

return [
    'managed-externally' => [
        'label'       => 'External Profile',
        'description' => 'This profile is managed in :source.',
        'action'      => 'Open profile in :source',
    ],
];
