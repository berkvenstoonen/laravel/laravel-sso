<?php

return [
    'managed-externally' => [
        'label'       => 'External Team',
        'description' => 'This team is managed in :source.',
        'action'      => 'Open team in :source',
    ],
];
