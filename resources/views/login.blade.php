<x-guest-layout>
    <x-authentication-card>
        <x-slot name="logo">
            <x-authentication-card-logo/>
        </x-slot>

        <x-validation-errors class="mb-4"/>

        <div class="w-full py-12 flex justify-center">
            <iframe style="width: 100%" src="{{ env('SSO_PARENT_SERVER_URL') }}/platforms/{{ env('SSO_PLATFORM_ID') }}/login/button/{{ app()->getLocale() }}" id="sso-frame"></iframe>
        </div>
    </x-authentication-card>
</x-guest-layout>
