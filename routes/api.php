<?php

use App\Models\Membership;
use App\Models\Team;
use App\Models\User;
use BeTo\Laravel\Exceptions\ProgrammingException;
use BeTo\LaravelSso\Actions\BulkRequest;
use BeTo\LaravelSso\Actions\InitialSetup;
use BeTo\LaravelSso\Http\Requests\CreateMembershipRequest;
use BeTo\LaravelSso\Http\Requests\CreateTeamRequest;
use BeTo\LaravelSso\Http\Requests\CreateUserRequest;
use BeTo\LaravelSso\Http\Requests\UpdateMembershipRequest;
use BeTo\LaravelSso\Http\Requests\UpdateTeamRequest;
use BeTo\LaravelSso\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

Route::prefix('api/platform/')->group(function () {
    Route::middleware('auth:platform')->group(function () {
        // Initial Setup
        Route::post('validate-setup', fn() => ['success'])->name('api.validate-setup');
        Route::post('initial-setup', [InitialSetup::class, 'setup'])->name('api.bulk');

        // Teams
        Route::post('teams', function (CreateTeamRequest $request) {
            return response()->json(Team::forceCreate($request->validationData()), 201);
        })->name('api.team.create');
        Route::put('teams/{team}', function (UpdateTeamRequest $request, int $teamId) {
            $team = Team::findOrFail($teamId);
            if ($team->update($request->validationData()) === false) {
                throw new ProgrammingException('Could not delete the model');
            }
            return response()->json($team->fresh());
        })->name('api.team.update');
        Route::delete('teams/{team}', function (int $teamId) {
            if (Team::findOrFail($teamId)->deleteOrFail() === false) {
                throw new ProgrammingException('Could not delete the model');
            }
            return response(status: 204);
        });

        // Users
        Route::post('users', function (CreateUserRequest $request) {
            $user = DB::transaction(function () use ($request) {
                $user = User::forceCreate($request->safe()->all());
                if ($user->deleteOrFail() === false) {
                    throw new ProgrammingException('Could not deactivate the newly created user');
                }
                return $user;
            });
            return response()->json($user, 201);
        })->name('api.user.create');
        Route::put('users/{user}', function (UpdateUserRequest $request, int $userId) {
            $user = User::withTrashed()->findOrFail($userId);
            $user->update($request->validationData());
            return response()->json($user->fresh());
        })->name('api.user.update');
        Route::delete('users/{user}', function (int $userId) {
            if (User::withTrashed()->findOrFail($userId)->forceDelete() === false) {
                throw new ProgrammingException('Could not delete the model');
            }
            return response(status: 204);
        });

        // Memberships
        Route::post('memberships', function (CreateMembershipRequest $request) {
            return response()->json(Membership::create($request->validationData()), 201);
        })->name('api.membership.create');
        Route::put('memberships/{membership}', function (UpdateMembershipRequest $request, int $membershipId) {
            $membership = Membership::findOrFail($membershipId);
            $membership->update($request->validationData());
            return response()->json($membership->fresh());
        })->name('api.membership.update');
        Route::delete('memberships/{membership}', function (int $membershipId) {
            $membership = Membership::findOrFail($membershipId);
            if ($membership->deleteOrFail() === false) {
                throw new ProgrammingException('Could not delete the model');
            }
            return response(status: 204);
        });

        // Bulk
        Route::post('bulk', [BulkRequest::class, 'bulk'])->name('api.bulk');

        // ForceSync
        Route::get('teams', function () {
            return response()->json(Team::pluck('name', 'id'));
        });
        Route::get('users', function () {
            return response()->json(User::withTrashed()->pluck('email', 'id'));
        });
        Route::delete('memberships', function () {
            foreach (Membership::all() as $membership) {
                $membership->delete();
            }
            return response(status: 204);
        });
    });
});
