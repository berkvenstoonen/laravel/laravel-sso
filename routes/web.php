<?php
declare(strict_types=1);

use App\Actions\Fortify\CreateNewUser;
use BeTo\Laravel\Exceptions\ProgrammingException;

Route::middleware(['web'])->group(function () {
    Route::view('/login', 'beto:sso::login')->name('login');

    Route::get('/validate-login/{key}', function (string $key, \Illuminate\Http\Request $request) {
        $response = Http::withHeaders(['Authorization' => 'Bearer ' . $key])->withOptions(['verify' => config('sso.verify-ssl')])->post(config('sso.platform.url') . '/api/validate-login/' . config('sso.platform.id'));
        if ($response->status() !== 200) {
            throw new ProgrammingException('Unexpected response from server');
        }

        $user = \App\Models\User::whereEmail($response->json('email'))->first();
        if ($user === null) {
            $user = CreateNewUser::getInstance()->create(
                [
                    'name'              => $response->json('name'),
                    'email'             => $response->json('email'),
                    'profile_photo_url' => $response->json('picture'),
                    'dark_mode'         => $response->json('dark_mode'),
                    'locale'            => $response->json('locale'),
                    'is_admin'          => $response->json('is_admin'),
                ],
            );
        }
        Auth::login($user);
        return redirect($request->query('redirect-url', '/'));
    });

    Route::middleware(['auth:sanctum', 'verified'])->group(function () {
        Route::get('/logout', function () {
            Auth::guard('web')->logout();
            return redirect('/');
        });
        Route::post('/logout')->name('logout-fallback');
        Route::domain(config('sso.platform.url'))->group(function () {
            Route::get('/user/profile')->name('profile.show-external');
            Route::get('/teams/{team}')->name('team.show-external');
            Route::post('/logout?' . http_build_query(['platform' => config('sso.platform.id')]))->name('logout');
        });
    });
});


