<?php
declare(strict_types=1);

return [
    'platform'   => [
        'id'  => env('SSO_PLATFORM_ID'),
        'key' => env('SSO_PLATFORM_KEY'),
        'url' => env('SSO_SERVER_URL'),
    ],
    'verify-ssl' => env('SSO_VERIFY_SSL', env('APP_ENV') !== 'local'),
];
