<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Console\Commands;

use App\Models\Team;
use Symfony\Component\Console\Attribute\AsCommand;
use Illuminate\Console\Command;

#[AsCommand(name: 'team:replace-id', description: 'Replace the id of a team with a different ID')]
final class ReplaceTeamId extends Command
{
    protected $signature = 'team:replace-id {current-id : The current ID of the team} {new-id : The ID you want to give to the team}';

    public function handle(): int
    {
        $newId = $this->argument('new-id');
        $team = Team::findOrFail($this->argument('current-id'));
        $found = Team::find($newId);
        if ($found !== null) {
            $this->output->writeln('<error>ID <comment>'.$newId.'</comment> is already in use by team <comment>'.$found->name.'</comment>');
            return 1;
        }
        \DB::transaction(function () use ($newId, $team) {
            \DB::update('SET foreign_key_checks = 0');
            \DB::update('UPDATE teams SET id = ? WHERE id = ?', [$newId, $team->id]);
            \DB::update('UPDATE users SET current_team_id = ? WHERE current_team_id = ?', [$newId, $team->id]);
            $found = \DB::select('SELECT DISTINCT TABLE_NAME, COLUMN_NAME FROM information_schema.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_NAME = ? AND REFERENCED_COLUMN_NAME = ?', ['teams', 'id']);
            foreach ($found as $foreignKey) {
                \DB::update('UPDATE '.$foreignKey->TABLE_NAME.' SET '.$foreignKey->COLUMN_NAME.' = ? WHERE '.$foreignKey->COLUMN_NAME.' = ?', [$newId, $team->id]);
            }
            $found = \DB::select('SELECT DISTINCT TABLE_NAME, COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ? AND COLUMN_NAME LIKE ?;', [env('DB_DATABASE'), '%team_id%']);
            foreach ($found as $foreignKey) {
                \DB::update('UPDATE '.$foreignKey->TABLE_NAME.' SET '.$foreignKey->COLUMN_NAME.' = ? WHERE '.$foreignKey->COLUMN_NAME.' = ?', [$newId, $team->id]);
            }
            \DB::update('SET foreign_key_checks = 1');
        });
        return 0;
    }
}
