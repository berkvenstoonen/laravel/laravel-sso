<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Http\Requests;

use App\Models\Team;
use BeTo\Laravel\Helpers\EmailRules;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

final class UpdateTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Route::current()->getName() === 'api.team.update';
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name'          => ['required', 'string', 'max:255'],
            'email'         => EmailRules::getUnique(false, team: Team::findOrFail($this->route('team'))),
            'user_id'       => ['required', 'int', Rule::exists('users', 'id')],
            'personal_team' => ['required', 'bool'],
        ];
    }
}
