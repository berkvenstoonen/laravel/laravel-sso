<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Http\Requests;

use BeTo\Laravel\Helpers\EmailRules;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

final class CreateTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Route::current()->getName() === 'api.team.create';
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name'          => ['required', 'string', 'max:255'],
            'email'         => EmailRules::getUnique(false),
            'user_id'       => ['required', 'int', Rule::exists('users', 'id')], // TODO @JB this needs to be ignored for the force sync
            'personal_team' => ['required', 'bool'],
        ];
    }
}
