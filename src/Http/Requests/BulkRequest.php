<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

final class BulkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Route::current()->getName() === 'api.bulk';
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            '*.method'  => ['required', 'string', Rule::in(['post', 'put', 'delete'])],
            '*.apiPath' => ['required', 'string', 'starts_with:team,user,membership'],
            '*.data'    => ['array'],
        ];
    }
}
