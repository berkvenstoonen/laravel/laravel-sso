<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Http\Requests;

use App\Models\User;
use BeTo\Laravel\Helpers\EmailRules;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

final class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Route::current()->getName() === 'api.user.update';
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name'              => ['required', 'string', 'max:255'],
            'email'             => EmailRules::getUnique(true, user: User::withTrashed()->findOrFail($this->route('user'))),
            'current_team_id'   => ['required', 'int', Rule::exists('teams', 'id')],
            'dark_mode'         => ['nullable', 'bool'],
            'locale'            => ['required', 'string', Rule::in(['nl', 'en'])],
            'is_admin'          => ['required', 'bool'],
            'profile_photo_url' => ['nullable', 'string'],
        ];
    }
}
