<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

final class UpdateMembershipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Route::current()->getName() === 'api.membership.update';
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'team_id' => ['required', 'int', Rule::exists('teams', 'id')],
            'user_id' => ['required', 'int', Rule::exists('users', 'id')],
            'role'    => ['required', 'string', Rule::in(['admin', 'editor', 'viewer'])],
        ];
    }
}
