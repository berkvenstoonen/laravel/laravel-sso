<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Http\Requests;

use BeTo\Laravel\Helpers\EmailRules;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

final class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Route::current()->getName() === 'api.user.create';
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name'              => ['required', 'string', 'max:255'],
            'email'             => EmailRules::getUnique(true),
            'current_team_id'   => ['required', 'int', Rule::exists('teams', 'id')],
            'dark_mode'         => ['nullable', 'bool'],
            'locale'            => ['required', 'string', Rule::in(['nl', 'en'])],
            'is_admin'          => ['required', 'bool'],
            'profile_photo_url' => ['nullable', 'string'],
        ];
    }
}
