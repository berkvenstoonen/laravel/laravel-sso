<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

final class InitialSetupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Route::current()->getName() === 'api.bulk';
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'user.name'              => ['required', 'string', 'max:255'],
            'user.email'             => ['required', 'string', env('APP_ENV') === 'testing' ? 'email' : 'email:rfc,dns,filter_unicode', 'max:255'],
            'user.current_team_id'   => ['required'],
            'user.dark_mode'         => ['nullable', 'bool'],
            'user.locale'            => ['required', 'string', Rule::in(['nl', 'en'])],
            'user.is_admin'          => ['required', 'bool'],
            'user.profile_photo_url' => ['nullable', 'string'],
            'teams.*.name'           => ['required', 'string', 'max:255'],
            'teams.*.email'          => ['nullable', 'string', env('APP_ENV') === 'testing' ? 'email' : 'email:rfc,dns,filter_unicode', 'max:255'],
            'teams.*.personal_team'  => ['required', 'bool'],
            'memberships.*.team_id'  => ['required'],
            'memberships.*.role'     => ['required', 'string', Rule::in(['admin', 'editor', 'viewer'])],
        ];
    }
}
