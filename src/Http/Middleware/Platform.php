<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Http\Middleware;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

final class Platform implements Guard
{
    public function __construct(
        private readonly Request $request,
    ) {
    }

    public function user(): ?Authenticatable
    {
        return null;
    }

    /**
     * @param array<string> $credentials
     */
    public function validate(array $credentials = []): bool
    {
        return true;
    }

    public function check(): bool
    {
        if ($this->request->header('Authorization') !== 'Bearer '.config('sso.platform.key')) {
            abort(401, 'Invalid API Key');
        }
        return true;
    }

    public function guest(): bool
    {
        return false;
    }

    public function id(): ?int
    {
        return null;
    }

    public function hasUser(): bool
    {
        return false;
    }

    public function setUser(Authenticatable $user): void
    {
    }
}
