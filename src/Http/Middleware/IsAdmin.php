<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Http\Middleware;

use BeTo\LaravelSso\Model\SsoUserInterface;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Webmozart\Assert\Assert;

final class IsAdmin
{
    public function handle(Request $request, Closure $next): Response|RedirectResponse
    {
        /** @var SsoUserInterface $user */
        $user = Auth::user();
        Assert::isInstanceOf($user, SsoUserInterface::class);
        if ($user?->isAdmin()) {
            return $next($request);
        }
        return redirect('/')->with('error', 'You do not have admin access');
    }
}
