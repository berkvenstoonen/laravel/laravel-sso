<?php

namespace BeTo\LaravelSso;

use BeTo\LaravelSso\Console\Commands\ReplaceTeamId;
use BeTo\LaravelSso\Http\Kernel;
use BeTo\LaravelSso\Http\Middleware\IsAdmin;
use BeTo\LaravelSso\Http\Middleware\Platform;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

final class SsoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->authentication();
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'beto:sso');
        $this->loadTranslationsFrom(__DIR__ . '/../lang', 'beto:sso');
        $this->commands([ReplaceTeamId::class]);
        $this->publishes(
            [
                __DIR__ . '/../config/sso.php'       => config_path('sso.php'),
                __DIR__ . '/../database/migrations/' => database_path('migrations'),
            ],
            'beto-sso'
        );
    }

    private function authentication(): void
    {
        App::singleton(
            \Illuminate\Contracts\Http\Kernel::class,
            Kernel::class
        );

        Auth::extend('admin', static function () {
            return new IsAdmin();
        });
        Auth::extend('platform', static function (Application $app) {
            return new Platform($app['request']);
        });

        Config::set('auth.guards.platform', [
            'driver'   => 'platform',
            'provider' => 'platforms',
            'hash'     => false,
        ]);
        Config::set('auth.providers.platforms', [
            'driver' => 'eloquent',
        ]);
    }
}
