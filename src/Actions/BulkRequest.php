<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Actions;

use App\Models\Membership;
use App\Models\Team;
use App\Models\User;
use BeTo\Laravel\Exceptions\GetRelevantExceptionData;
use BeTo\Laravel\Exceptions\ProgrammingException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

final class BulkRequest
{
    private array $response;
    private int   $currentRequestId;

    public function bulk(\BeTo\LaravelSso\Http\Requests\BulkRequest $request): JsonResponse
    {
        try {
            $response = DB::transaction(function () use ($request) {
                $this->response = [];
                foreach ($request->validationData() as $requestId => $requestData) {
                    $this->currentRequestId = $requestId;
                    switch ($requestData['method']) {
                        case 'post':
                            $this->response[] = $this->create($requestData['apiPath'], $requestData['data'])->toArray();
                            break;
                        case 'put':
                            $this->response[] = $this->update($this->replaceBulkId($requestData['apiPath']), $requestData['data'])->toArray();
                            break;
                        case 'delete':
                            $this->delete($requestData['apiPath']);
                            $this->response[] = null;
                    }
                }
                return $this->response;
            });
        } catch (\Throwable $exception) {
            $response = $this->getErrorResponse($exception);
        }
        return response()->json($response, 201);
    }

    private function create(string $apiPath, array $data): Model
    {
        $data = $this->replaceBulkIds($data);
        return match ($apiPath) {
            'users'       => User::createFromData($data),
            'teams'       => Team::createFromData($data),
            'memberships' => Membership::createFromData($data),
            default       => throw new ProgrammingException('Unknown create action: ' . $apiPath),
        };
    }

    private function update(string $apiPath, array $data): Model
    {
        $data = $this->replaceBulkIds($data);
        preg_match('/(users|teams|memberships)\/([0-9]+)/', $apiPath, $matches);
        return match ($matches[1]) {
            'users'       => User::findOrFail($matches[2])->forceFill($data),
            'teams'       => Team::findOrFail($matches[2])->forceFill($data),
            'memberships' => Membership::findOrFail($matches[2])->forceFill($data),
            default       => throw new ProgrammingException('Unknown update action: ' . $apiPath),
        };
    }

    private function delete(string $apiPath): void
    {
        preg_match('/(users|teams|memberships)\/([0-9]+)/', $apiPath, $matches);
        switch ($matches[1] ?? null) {
            case 'users':
                User::findOrFail($matches[2])->delete();
                break;
            case 'teams':
                Team::findOrFail($matches[2])->delete();
                break;
            case 'memberships':
                Membership::findOrFail($matches[2])->delete();
                break;
            default:
                throw new ProgrammingException('Unknown update action: ' . $apiPath);
        }
    }

    private function getErrorResponse(\Throwable $exception): array
    {
        $data = [
            'error'        => true,
            'message'      => $exception->getMessage(),
            'requestIndex' => $this->currentRequestId ?? null,
        ];
        if (env('APP_DEBUG', false)) {
            $relevantData               = GetRelevantExceptionData::getRelevantData($exception);
            $data['relevantTraces']     = array_shift($relevantData)['relevantTraces'];
            $data['previousExceptions'] = $relevantData;
        }
        return $data;
    }

    private function replaceBulkIds(array $data): array
    {
        $replacedData = [];
        foreach ($data as $key => $value) {
            if (is_string($value) && str_starts_with($value, 'bulkId:')) {
                $value = $this->replaceBulkId($value);
            }
            $replacedData[$key] = $value;
        }
        return $replacedData;
    }

    private function replaceBulkId(string $value): string
    {
        if (!preg_match('/bulkId:([0-9]+)/', $value, $matches)) {
            return $value;
        }
        $id = $this->response[(int) $matches[1]]['id'];
        return str_replace($matches[0], (string) $id, $value);
    }
}
