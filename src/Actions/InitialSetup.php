<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Actions;

use App\Models\Membership;
use App\Models\Team;
use App\Models\User;
use BeTo\Laravel\Exceptions\GetRelevantExceptionData;
use BeTo\LaravelSso\Http\Requests\InitialSetupRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

final class InitialSetup
{
    private array $response;
    private int   $currentTeamBulkId;
    private int   $userId;
    private array $teamIdMapping;

    public function setup(InitialSetupRequest $request): JsonResponse
    {
        try {
            $response = DB::transaction(function () use ($request) {
                $data             = $request->validationData();
                $this->response   = [];
                return response()->json($data, 201);
                $user             = User::createFromData(['current_team_id' => 0] + $data['user']);
                $this->userId     = $user->id;
                $this->response['user'] = $user->toArray();
                foreach ($data['teams'] as $teamBulkId => $requestData) {
                    $this->currentTeamBulkId          = $teamBulkId;
                    $team                             = Team::createFromData($this->replaceBulkIds($requestData));
                    $this->teamIdMapping[$teamBulkId] = $team->id;
                    $this->response['teams'][]                 = $team->toArray();
                }
                foreach ($data['memberships'] ?? [] as $teamBulkId => $requestData) {
                    $this->currentTeamBulkId          = $teamBulkId;
                    $team                             = Membership::createFromData($this->replaceBulkIds($requestData));
                    $this->teamIdMapping[$teamBulkId] = $team->id;
                    $this->response[]                 = $team->toArray();
                }
                $user->update(['current_team_id' => $this->teamIdMapping[$user->current_team_id]]);
                return $this->response;
            });
        } catch (\Throwable $exception) {
            return response()->json($this->getErrorResponse($exception), 500);
        }
        return response()->json($response, 201);
    }

    private function getErrorResponse(\Throwable $exception): array
    {
        $data = [
            'error'        => true,
            'message'      => $exception->getMessage(),
            'requestIndex' => $this->currentTeamBulkId ?? null,
        ];
        if (env('APP_DEBUG', false)) {
            $relevantData               = GetRelevantExceptionData::getRelevantData($exception);
            $data['relevantTraces']     = array_shift($relevantData)['relevantTraces'];
            $data['previousExceptions'] = $relevantData;
        }
        return $data;
    }

    private function replaceBulkIds(array $data): array
    {
        $replacedData = [];
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = $this->replaceBulkIds($value);
            } elseif (is_string($value)) {
                $value = $this->replaceBulkId($value);
            }
            $replacedData[$key] = $value;
        }
        return $replacedData;
    }

    private function replaceBulkId(string $value): string|int
    {
        if (preg_match('/teamId:([0-9]+)/', $value, $matches)) {
            return $this->teamIdMapping[(int) $matches[1]];
        } elseif ($value === 'userId') {
            return $this->userId;
        }
        return $value;
    }
}
