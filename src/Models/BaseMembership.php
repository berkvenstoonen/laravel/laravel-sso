<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Laravel\Jetstream\Membership;
use Sentry\Serializer\SerializableInterface;

/**
 * \BeTo\LaravelSso\Models\BaseMembership
 *
 * @property int $id
 * @property int $team_id
 * @property int $user_id
 * @property string|null $role
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|\App\Models\Membership newModelQuery()
 * @method static Builder|Membership newQuery()
 * @method static Builder|Membership query()
 * @method static Builder|Membership whereCreatedAt($value)
 * @method static Builder|Membership whereId($value)
 * @method static Builder|Membership whereRole($value)
 * @method static Builder|Membership whereTeamId($value)
 * @method static Builder|Membership whereUpdatedAt($value)
 * @method static Builder|Membership whereUserId($value)
 * @mixin \Eloquent
 */
abstract class BaseMembership extends Membership implements SerializableInterface, SsoInterface
{
    use SsoTrait;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    public function toSentry(): ?array
    {
        return $this->toArray();
    }
}
