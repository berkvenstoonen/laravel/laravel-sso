<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sentry\Serializer\SerializableInterface;

/**
 * \BeTo\LaravelSso\Models\BaseUser
 *
 * @property bool $is_admin
 * @method static Builder|static whereIsAdmin($value)
 */
abstract class BaseUser extends \BeTo\Laravel\Models\BaseUser implements SerializableInterface, SsoUserInterface
{
    use SsoUserTrait;
    use SsoTrait;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'name',
        'email',
        'locale',
        'is_admin',
        'dark_mode',
        'current_team_id',
        'profile_photo_url',
    ];
    protected $casts    = [
        'is_admin'  => 'boolean',
        'dark_mode' => 'boolean',
    ];
}
