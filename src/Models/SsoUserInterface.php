<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Models;

interface SsoUserInterface extends SsoInterface
{
    public function isAdmin(): bool;

    public function getId(): int;

    public function getDisplayName(): string;

    public function setDisplayName(string $displayName): static;

    public function getEmail(): string;

    public function setEmail(string $email): static;

    public function getProfilePhotoPath(): ?string;

    public function setProfilePhotoPath(?string $profilePhotoPath): static;

    public function getProfilePhotoUrl(): ?string;

    public function getLocale(): string;

    public function setLocale(string $locale): static;

    public function getDarkMode(): ?bool;

    public function setDarkMode(?bool $darkMode): static;
}
