<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Models;

trait SsoUserTrait
{
    public function isAdmin(): bool
    {
        return $this->exists && $this->is_admin;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDisplayName(): string
    {
        return $this->name;
    }

    public function setDisplayName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;
        return $this;
    }

    public function getProfilePhotoPath(): ?string
    {
        return $this->profile_photo_path;
    }

    public function setProfilePhotoPath(?string $profilePhotoPath): static
    {
        $this->profile_photo_path = $profilePhotoPath;
        return $this;
    }

    public function getProfilePhotoUrl(): ?string
    {
        return $this->profile_photo_url;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): static
    {
        $this->locale = $locale;
        return $this;
    }

    public function getDarkMode(): ?bool
    {
        return $this->dark_mode;
    }

    public function setDarkMode(?bool $darkMode): static
    {
        $this->dark_mode = $darkMode;
        return $this;
    }
}
