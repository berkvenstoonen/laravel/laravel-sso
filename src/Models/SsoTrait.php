<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

trait SsoTrait
{
    public static function createFromData(array $data): static
    {
        if (array_key_exists('id', $data) && in_array(SoftDeletes::class, class_uses_recursive(static::class))) {
            /** @var User $deletedUser */
            $deletedUser = static::onlyTrashed()->find($data['id']);
            if ($deletedUser !== null) {
                $deletedUser->restore();
                $deletedUser->update($data);
                return $deletedUser;
            }
        }
        return static::forceCreate($data);
    }
}
