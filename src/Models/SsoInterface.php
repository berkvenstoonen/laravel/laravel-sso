<?php
declare(strict_types=1);

namespace BeTo\LaravelSso\Models;

interface SsoInterface
{
    public static function createFromData(array $data): static;
}
